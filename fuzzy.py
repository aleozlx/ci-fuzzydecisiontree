# Definitions of some mathematical concepts
Tri = lambda a,b,c: lambda x: ((x-a)/(b-a) if x<b else (x-c)/(b-c)) if a<x<c else 0.0
Trap = lambda a,b1,b2,c: lambda x: ((x-a)/(b1-a) if x<b1 else ((x-c)/(b2-c) if x>b2 else 1.0)) if a<x<c else 0.0
operators = {
    'zadeh': (
        lambda A: lambda x: 1.0-A(x), # NOT
        lambda A,B: lambda x: max(A(x),B(x)), # OR
        lambda A,B: lambda x: min(A(x),B(x)), # AND
    ),
    'alternative': (
        lambda A: lambda x: 1.0-A(x), # NOT
        lambda A,B: lambda x: min(1.0, A(x)+B(x)), # OR
        lambda A,B: lambda x: max(0.0, A(x)+B(x)-1.0), # AND
    ),
    'yager': lambda w: (
        lambda A: lambda x: pow(1.0-pow(A(x),w),1.0/w), # NOT
        lambda A,B: lambda x: min(1.0, pow(pow(A(x),w)+pow(B(x),w),1.0/w)), # OR
        lambda A,B: lambda x: 1.0-min(1.0, pow(pow(1.0-A(x),w)+pow(1.0-B(x),w),1.0/w)), # AND
    ),
}

# PART A
from collections import namedtuple
# Define data structure and load data
Car = namedtuple('Car', 'id risk value_loss horsepower city_mpg highway_mpg price')
with open('datafiles/car_data.csv') as f:
    car_data = [Car(*map(float,line.split(','))) for line in filter(len, (line.strip() for line in f.readlines()))]
# Some constants indexing appropiate membership functions
LOW, AVG, HIGH = range(3); POOR, GOOD = LOW, HIGH
# Define membership functions that apply to cars
risk = lambda level: lambda car: (Trap(-3, -3, -2, 0), Tri(-2, 0, 2), Trap(0, 2, 3, 3))[level](car.risk)
city_mpg = lambda level: lambda car: (Trap(0, 0, 20, 30), Tri(20, 30, 40), Trap(30, 40, 60, 60))[level](car.city_mpg)
value_loss = lambda level: lambda car: (Trap(0, 0, 100, 120), Tri(100, 120, 200), Trap(120, 200, 300, 300))[level](car.value_loss)
highway_mpg = lambda level: lambda car: (Trap(0, 0, 20, 30), Tri(20, 30, 40), Trap(30, 40, 60, 60))[level](car.highway_mpg)
horsepower = lambda level: lambda car: (Trap(0, 0, 60, 100), Tri(60, 100, 140), Trap(100, 140, 250, 250))[level](car.horsepower)
price = lambda level: lambda car: (Trap(0, 0, 7000, 10000), Tri(7000, 10000, 20000), Trap(10000, 20000, 40000, 40000))[level](car.price)

def gen_hist(fname, data):
    """Generate data for plotting score histogram"""
    from json import dump
    # from collections import Counter
    # with open(fname, 'w') as f: dump(sorted(Counter(data).items()), f)
    with open(fname, 'w') as f: dump(data, f)

def print_ranking((NOT, OR, AND), top=5, hist=None):
    """Print top 5 cars based on the fuzzy decision tree"""
    fuzzy_decision = AND(
        AND(AND(NOT(city_mpg(POOR)),highway_mpg(GOOD)),horsepower(AVG)),
        OR(AND(risk(LOW),value_loss(LOW)),price(LOW)))
    print '============='
    score = sorted(((fuzzy_decision(car), car) for car in car_data), reverse=True)
    for car in score[:top]:
        print car
    if hist: gen_hist(hist, map(lambda s:round(s[0],2), score))

# (NOT, OR, AND)= operators['zadeh']
# fuzzy_decision = AND(
#     AND(AND(NOT(city_mpg(POOR)),highway_mpg(GOOD)),horsepower(AVG)),
#     OR(AND(risk(LOW),value_loss(LOW)),price(LOW)))
# print [f(car_data[0]) for f in [city_mpg(POOR),highway_mpg(GOOD),horsepower(AVG),risk(LOW),value_loss(LOW),price(LOW)]]
# sys.exit()

print_ranking(operators['zadeh'], hist='hist.json')
print_ranking(operators['alternative'])
for w in [.1, 1., 1E1, 1E2, 1E3, 1E4]:
    print_ranking(operators['yager'](w))

# PART B
"""Quadratic membership from textbook"""
S = lambda a,b,c: lambda x: 0.0 if x<=a else (
    (x-a)**2/(2*(b-a)**2) if x<=b else(
        1-(x-c)**2/(2*(b-c)**2) if x<=c else 1.0))

def SoftTri(a,b,c):
    """Tri function constructed using S(x;a,b,c) instead of straight lines"""
    uphill = S(a,(a+b)/2.0,b); downhill = lambda x:1.0-S(b,(b+c)/2.0,c)(x)
    def r(x):
        if x<=a: return 0.0
        elif x<=b: return uphill(x)
        elif x<=c: return downhill(x)
        else: return 0.0
    return r

def SoftTrap(a,b1,b2,c):
    """Trap function constructed using S(x;a,b,c) instead of straight lines"""
    uphill = S(a,(a+b1)/2,b1); downhill = lambda x:1.0-S(b2,(b2+c)/2.0,c)(x)
    def r(x):
        if x<=a: return 0.0
        elif x<=b1: return uphill(x)
        elif x<=b2: return 1.0
        elif x<=c: return downhill(x)
        else: return 0.0
    return r

def part_B():
    """Same paramaters as Part I with SoftTrap/SoftTri membership functions"""
    risk = lambda level: lambda car: (SoftTrap(-3, -3, -2, 0), SoftTri(-2, 0, 2), SoftTrap(0, 2, 3, 3))[level](car.risk)
    city_mpg = lambda level: lambda car: (SoftTrap(0, 0, 20, 30), SoftTri(20, 30, 40), SoftTrap(30, 40, 60, 60))[level](car.city_mpg)
    value_loss = lambda level: lambda car: (SoftTrap(0, 0, 100, 120), SoftTri(100, 120, 200), SoftTrap(120, 200, 300, 300))[level](car.value_loss)
    horsepower = lambda level: lambda car: (SoftTrap(0, 0, 60, 100), SoftTri(60, 100, 140), SoftTrap(100, 140, 250, 250))[level](car.horsepower)
    price = lambda level: lambda car: (SoftTrap(0, 0, 7000, 10000), SoftTri(7000, 10000, 20000), SoftTrap(10000, 20000, 40000, 40000))[level](car.price)
    NOT, OR, AND = operators['zadeh']
    fuzzy_decision = AND(
        AND(NOT(city_mpg(GOOD)),horsepower(AVG)),
        OR(AND(risk(AVG),NOT(value_loss(HIGH))),price(LOW)))
    print 'PART B ======'
    score = sorted(((fuzzy_decision(car), car) for car in car_data), reverse=True)
    for car in score[:10]:
        print car
    print map(lambda s:round(s[0],2), score)

part_B()

