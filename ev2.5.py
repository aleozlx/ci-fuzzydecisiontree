import numpy as np
N, D = 100, 2
X=(np.random.rand(N, D)*20)-10

def mutate(X, alpha=1.):
    return np.concatenate((X,X+alpha*np.random.rand(*X.shape)))

def hard_select(f, X):
    F = apply(f, [X.T[di] for di in xrange(D)])
    I = F.argsort()[:N]
    return X[I], np.linalg.norm(F[I])

for i in xrange(40):
    f = lambda x,y: np.power(x,2)-10*np.cos(6.283185307179586*x)+np.power(y,2)-10*np.cos(6.283185307179586*y)+20
    X, fitness = hard_select(f, mutate(X))
    print i, fitness
