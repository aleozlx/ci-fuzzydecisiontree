import numpy as np
population = 100
X=(np.random.rand(population)*20)-10
Y=(np.random.rand(population)*20)-10
def f(x, y):
    return (np.power(x,2)-10*np.cos(6.283185307179586*x) +
        np.power(y,2)-10*np.cos(6.283185307179586*y) + 20)

def mutate(x):
    return np.concatenate((x,x+np.random.rand(population)))

for i in xrange(40):
    X2, Y2=mutate(X), mutate(Y)
    R=np.sort(np.column_stack((f(X2, Y2), X2, Y2)).view('f8,f8,f8'), axis=0, order='f0')[:population]
    X, Y=R['f1'].ravel(), R['f2'].ravel()
    print i, np.linalg.norm(R['f0'])
