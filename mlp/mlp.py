import random
import numpy as np
import scipy.special
sigmoid = [scipy.special.expit, lambda x,y=0: y*(1.0-y)]

class MultilayerPerceptron(object):
    """Multilayer perceptron (OOP-style implementation)
        DEPRECATED because random initialization of weights wasn't done properly 
        and OOP makes it slow."""
    def __init__(self, layers):
        super(MultilayerPerceptron, self).__init__()
        self.layers = layers
        for i in xrange(len(layers)-1):
            for neuron in layers[i]: # give each hidden layer neuron reference to next layer
                neuron.nextlayer = layers[i+1]
            
    def __call__(self, x):
        """MLP forward pass"""
        for layer in self.layers:
            x = np.array([neuron(x) for neuron in layer]) # replace x with output as next input
        self.y = x # store final output
        return self.y
    
    def train1(self, error, rate):
        """MLP backward pass"""
        for layer in reversed(self.layers):
            for j, neuron in enumerate(layer):
                neuron.update_delta(j, error)
        for layer in self.layers:
            for neuron in layer:
                neuron.update_weights(rate)
            
class Neuron(object):
    def __init__(self, activaction, w):
        super(Neuron, self).__init__()
        # random weight vector (including bias) with w elems, or w is the weight vector
        self.weights = np.random.rand(w) if isinstance(w, int) else w
        self.phi = activaction # activaction function & derivatives
        self.nextlayer = [] # neuron is on output layer by default
        self.y = 0
    
    def __call__(self, x):
        """Calculate & store neuron input/output"""
        # if x.shape[0]+1!=self.weights.shape[0]:
        #     print 'ERR:', x.shape[0], self.weights.shape[0]
        self.x, self.y = x, self.phi[0](np.dot(self.weights, np.append(x,[1.]))) # a = phi(wT x)
        return self.y
        
    def update_delta(self, j, e=None):
        """Calculate & store partial derivative of error energy with respect to induced local field
         @param j neuron index within the layer
         @param e error for output layer neuron"""
        self.delta = (self.phi[1](self.x, self.y) # 1st order derivative of activation function a.k.a (phi'(v))
            * (sum(neuron.delta * neuron.weights[j] for neuron in self.nextlayer) if self.nextlayer else e[j]))
    
    def update_weights(self, rate):
        """Update weights"""
        self.weights += (rate * self.delta) * np.append(self.x, [1.])
        
class vMLP(object):
    """Vectorized multilayer perceptron supporting vectorized batch update"""
    def __init__(self, layers):
        super(vMLP, self).__init__()
        self.layers = layers
        for i in xrange(len(layers)-1):
            layers[i].nextlayer = layers[i+1]

    def __call__(self, x):
        """MLP forward pass"""
        for layer in self.layers:
            x = layer(x)
        self.y = x # store final output
        return self.y
    
    def train(self, t, rate):
        """MLP backward pass"""
        for layer in reversed(self.layers):
            layer.update_delta(t)
        for layer in self.layers:
            layer.update_weights(rate)
    
    @staticmethod    
    def shortcut(activaction, *n):
        L = lambda nx, ny: NeuronLayer(activaction, nx+1, ny)
        return vMLP([L(n[i], n[i+1]) for i in xrange(len(n)-1)])
        
    @staticmethod
    def prepare(x, t, normalization = True):
        x = np.array([row+[1.] for row in x])
        if normalization:
            # _x = x[:,:-1]
            # _x = (_x - np.mean(_x, axis=0)) / np.std(_x, axis=0, ddof=1)
            
            from sklearn.preprocessing import scale
            scale(x[:,:-1], copy=False)
        return Dataset(x, np.array(list(t)))
        
class NeuronLayer(object):
    """Layer of homogeneous neurons in a vectorized implementation"""
    def __init__(self, activaction, w, n=None):
        super(NeuronLayer, self).__init__()
        if isinstance(w, int):
            # weights [w,n]
            self.weights = (2*np.random.rand(w, n)-1)/np.sqrt(w) if isinstance(w, int) else w.T.copy()
            self.momentum = np.zeros((w, n))
        else:
            # weights = w.T = [ n1 n2 n3 ... ]
            self.weights = w.T.copy()
            self.momentum = np.zeros_like(self.weights)
        self.phi = activaction # activaction function & derivatives
        self.nextlayer = None
        self.y = np.array(0)
        
    def __call__(self, x):
        self.x = x
        if self.nextlayer:
            # y [N,n+1] = [phi(x [N,w] weights [w,n]) (ones for biases)]
            self.y = np.empty((x.shape[0], self.weights.shape[1]+1), dtype = float)
            self.y[:,:-1] = self.phi[0](np.dot(x, self.weights))
            self.y[:,-1] = 1.
        else:
            # y [N,n] = phi(x [N,w] weights [w,n])
            self.y = self.phi[0](np.dot(x, self.weights))
        return self.y
    
    def update_delta(self, t=None):
        # delta [N,n] = phi'(v) [N,n] * (error term) [N,n]
        if self.nextlayer:
            # biases from next layer has no feedback towards this layer hence [:,:-1]
            self.delta = (self.phi[1](self.x, self.y[:,:-1]) * 
                np.dot(self.nextlayer.delta, self.nextlayer.weights.T[:,:-1]))
        else:
            self.delta = self.phi[1](self.x, self.y) * (t - self.y)
            
    def update_weights(self, rate, momentum=0):
        # weights [w,n] += rate * 1/N * (delta [N,n] * x [N,w])
        d = ((rate/self.x.shape[0]) * 
            # np.einsum('Nn,Nw->wn', self.delta, self.x)) # Damn, Einstein sum makes so much sense
            np.dot(self.x.T, self.delta)) # 2.1x speedup if linked against BLAS and be less stupid
        self.weights += d
        if momentum:
            self.weights += momentum * self.momentum
            self.momentum = d

from collections import namedtuple
Dataset = namedtuple('Dataset', ['x', 't'])

class Error(namedtuple('Error', ['training', 'validation'])):
    @staticmethod
    def mean(e):
        return Error(*tuple(np.mean(np.asarray(e), axis=0)))

_onehot_cache = dict()

def onehot(i,n):
    if n not in _onehot_cache:
        _onehot_cache[n] = np.identity(n)
    return _onehot_cache[n][i]
    
import re
def load_csv(fname):
    raw_dataset = list()
    with open(fname) as f:
        for i, ll in enumerate(f.readlines()):
            line = ll.strip()
            if line:
                try:
                    raw_dataset.append([float(s.strip()) for s in re.split(r',|\s+',line)])
                except ValueError:
                    print ('Malformed line[%d]:'%(i+1)), line
    random.shuffle(raw_dataset)
    return raw_dataset
    