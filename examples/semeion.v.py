import random, itertools
import numpy as np
import scipy.special
from collections import namedtuple
from mlp import vMLP
with open('datafiles/semeion.data') as f:
    raw_dataset = [[float(s.strip()) for s in line.split()] for line in f.readlines()]
    random.shuffle(raw_dataset)

Dataset = namedtuple('Dataset', ['x', 't'])

def prepare(dataset, normalization = False):
    x = np.array([row[:256]+[1.] for row in dataset])
    t = np.array([row[256:] for row in dataset])
    if normalization:
        x = (x - np.mean(x, axis=0)) / np.std(x, axis=0)
    return Dataset(x, t)

sigmoid = [scipy.special.expit, lambda x,y=0: y*(1.0-y)]

# mlp = vMLP.shortcut(sigmoid, 256, 12, 10); r0 = 0.1
mlp = vMLP.shortcut(sigmoid, 256, 10); r0 = 0.1
datasets = training, validation, testing = prepare(raw_dataset[:956]), prepare(raw_dataset[956:1275]), prepare(raw_dataset[1275:])

e2T = list() # squared error for training
e2V = list() # squared error for validation

def updateMLP(dataset, rate):
    mlp(dataset.x)
    mlp.train(dataset.t, rate)

def evaluateMLP(dataset):
    # e = dataset.t - mlp(dataset.x)
    # print e
    # quit()
    # return np.sum(e**2)
    return np.sum((dataset.t - mlp(dataset.x))**2)

def driver():
    for j in xrange(5000):
        updateMLP(training, r0)
        e2T.append(evaluateMLP(training))
        e2V.append(evaluateMLP(validation))
        if j>250 and e2V[j]>=e2V[j-100]:
            break
        if j%100==99:
            # print e2V
            print 'iter', j, 'T', np.mean(e2T)/training.x.shape[0], 'V', np.mean(e2V)/validation.x.shape[0]
    
    # Classification accuracy
    N = map(lambda D: np.sum(np.argmax(mlp(D.x), axis=1)==np.argmax(D.t, axis=1)), datasets)
    accuracy = [float(nC)/D.x.shape[0] for nC, D in zip(N, datasets)] + [float(sum(N))/len(raw_dataset)]
    print 'Classification accuracy:  train - %.2f   validation - %.2f  test - %.2f  all - %.2f\n' % tuple(accuracy)

# driver()

import cProfile
cProfile.run('driver()', sort='tottime')

