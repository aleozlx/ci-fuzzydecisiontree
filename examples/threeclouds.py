from mlp import *
with open('datafiles/threeclouds.data') as f:
    import random
    raw_dataset = [[float(s.strip()) for s in line.split(',')] for line in f.readlines()]
    random.shuffle(raw_dataset)

def prepare(dataset):
    targets = np.array([
        [1., -1. ,-1],
        [-1., 1. ,-1],
        [-1., -1. ,1]
    ])
    return map(lambda row: (np.array(row[1:]), targets[row[0]-1], row[0]-1), dataset)

training = prepare(raw_dataset[:int(0.8*len(raw_dataset))])
validation = prepare(raw_dataset[int(0.8*len(raw_dataset)):int(0.9*len(raw_dataset))])
test = prepare(raw_dataset[int(0.9*len(raw_dataset)):])

tanh = [np.tanh, lambda x,a=0: 1.0-a*a]
N = lambda w: Neuron(tanh, w)
mlp = MultilayerPerceptron([
    [N(3), N(3), N(3)], 
    [N(4), N(4), N(4)]
])

def run(dataset, rate=0):
    loss = 0
    for x, t, label in dataset:
        y = mlp(x)
        if np.argmax(y)!=label:
            loss += 1
        e = t - y
        mlp.train1(e, rate)
    return loss*100.0/len(dataset)
        
def driver():
    r0 = 0.3
    for i in xrange(12):
        e1, e2 = run(training, r0/(1.0+i*r0)), run(validation)
        print 'T', e1, 'V', e2
    print 'Accuracy: %.2f%%' % np.round(run(test), 2)

driver()
