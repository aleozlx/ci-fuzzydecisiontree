import itertools, sys
import numpy as np
from mlp import vMLP, sigmoid, Error, onehot, load_csv

if len(sys.argv) <= 1:
    print 'Specify intput data.'
    sys.exit()

raw_dataset = load_csv(sys.argv[1])
setup = {
    'datafiles/semeion.data': lambda: dict(
        prepare = lambda dataset: vMLP.prepare((row[:256] for row in dataset), (row[256:] for row in dataset), normalization=False),
        datasets = (raw_dataset[:956], raw_dataset[956:1275], raw_dataset[1275:]),
        network = vMLP.shortcut(sigmoid, 256, 10), learning_rate = 0.1
    ),
    'datafiles/wine.data': lambda: dict(
        prepare = lambda dataset: vMLP.prepare((row[1:] for row in dataset), (onehot(int(row[0])-1,3) for row in dataset)),
        datasets = (raw_dataset[:106], raw_dataset[106:142], raw_dataset[142:]),
        network = vMLP.shortcut(sigmoid, 13, 12, 3), learning_rate = 0.1
    ),
    'datafiles/threeclouds.data': lambda: dict(
        prepare = lambda dataset: vMLP.prepare((row[1:] for row in dataset), (onehot(int(row[0])-1,3) for row in dataset)),
        datasets = (raw_dataset[:180], raw_dataset[180:240], raw_dataset[240:]),
        network = vMLP.shortcut(sigmoid, 2, 3), learning_rate = 0.1
    )
}[sys.argv[1]]()

datasets = training, validation, testing = map(setup['prepare'], setup['datasets'])
mlp = setup['network']; r0 = setup['learning_rate']
e2 = list() # squared errors for training/validation
# evaluateMLP = lambda dataset: np.sum((dataset.t - mlp(dataset.x))**2)
def evaluateMLP(dataset):
    e=dataset.t - mlp(dataset.x)
    # print np.sum(e**2)
    # print len(dataset.x)
    # print len(raw_dataset)
    # sys.exit()
    return np.sum(e**2)
for j in xrange(5000):
    mlp(training.x)
    mlp.train(training.t, r0)
    e2.append(Error(evaluateMLP(training), evaluateMLP(validation)))
    if j>250 and e2[j].validation>=e2[j-100].validation: break
    if j%100==99:
        e2mean = Error.mean(e2)
        # print 'iter', j, 'T', e2mean.training/training.x.shape[0], 'V', e2mean.validation/validation.x.shape[0], 'e2T', e2[j].training, 'len(T)', training.x.shape[0]
        print 'iter', j, 'T', e2[j].training/training.x.shape[0], 'V', e2[j].training/validation.x.shape[0], 'e2T', e2[j].training, 'len(T)', training.x.shape[0]

# Classification accuracy
N = map(lambda D: np.sum(np.argmax(mlp(D.x), axis=1)==np.argmax(D.t, axis=1)), datasets)
accuracy = [float(nC)/D.x.shape[0] for nC, D in zip(N, datasets)] + [float(sum(N))/len(raw_dataset)]
print 'Classification accuracy:  train - %.2f   validation - %.2f  test - %.2f  all - %.2f\n' % tuple(accuracy)
