clear; 

K = 25;        % Number of parents
maxIter = 40;  % Max iterations

Xp = (rand([K 1])*20.0) - 10;  % Randomly selected initial population from [-10, 10]
bScore = [];                   % Keep track of best result at each iteration

for i = 1:maxIter
    
    
    %bScore(end+1) = 
end

disp(['Final Result: ', num2str(bScore(end))]);

figure;
plot(bScore);
xlabel('iteration');
ylabel('minimum fitness');